//
//  NetManager.swift
//  HiberusTest
//
//  Created by Michele Rauzino on 17/3/21.
//

import Alamofire
import CryptoKit

class NetManager {
    
    static let baseURL = "https://gateway.marvel.com:443/v1/public/characters"
    static let pubAPI = "ab93bd3ec57a0b616a401541559cd36e"
    static let privAPI = "fb9ea941bb0fc9934f92d814e3ca1036494bc088"
    
    class func requestData(requestID: Int, offset: Int, limit: Int, onSuccess: @escaping (Character) -> Void, onFailure: @escaping (String) -> Void) {
        let timestamp = Int(Date().timeIntervalSince1970)
        let hashData = Insecure.MD5.hash(data: String(format: "%d%@%@", timestamp,privAPI,pubAPI).data(using: .utf8)!)
        let hash = hashData.map { String(format: "%02hhx", $0) }.joined()
        var finalURL : String
        
        if requestID != -1 {
            finalURL = String(format: "%@/%d?ts=%d&apikey=%@&hash=%@", baseURL, requestID, timestamp, pubAPI, hash)
        } else {
            finalURL = String(format: "%@?limit=%d&offset=%d&ts=%d&apikey=%@&hash=%@", baseURL, limit, offset, timestamp, pubAPI, hash)
        }
        
        AF.request(finalURL).validate().responseDecodable(of: Character.self) { (response) in
            switch response.result {
            case .success:
                if let characters = response.value {
                    onSuccess(characters)
                }
            case .failure(let error):
                if let httpStatusCode = response.response?.statusCode {
                    let message : String
                    switch httpStatusCode {
                    case 401:
                        message = "Unauthorized"
                    case 500:
                        message = "Internal Server Error"
                    case 503:
                        message = "Service Unavailable"
                    case 504:
                        message = "Gateway Timeout"
                    default:
                        message = "Network Error"
                    }
                    onFailure(message)
                }
                onFailure(error.localizedDescription)
            }
        }
    }
}
