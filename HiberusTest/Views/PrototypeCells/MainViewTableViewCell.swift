//
//  MainViewTableViewCell.swift
//  HiberusTest
//
//  Created by Michele Rauzino on 18/3/21.
//

import UIKit

class MainViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var charImage: UIImageView!
    @IBOutlet weak var charNameLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
