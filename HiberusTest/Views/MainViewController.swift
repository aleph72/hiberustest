//
//  MainViewController.swift
//  HiberusTest
//
//  Created by Michele Rauzino on 17/3/21.
//

import UIKit
import SDWebImage

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navTitle: UINavigationItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let limit = 40 //defines how many characters are fetched each time
    let newFetchIndex = 10 //defines how many characters are left to show before fetching new ones
    
    var characters: [Result] = [] {
        didSet {
            if characters.count > 0 {
                activityIndicator.stopAnimating()
                tableView.isHidden = false
                tableView.reloadData()
            } else {
                tableView.isHidden = true
                activityIndicator.startAnimating()
            }
        }
    }
    var response: Character?
    var fromIndex = 0
    var selectedCharacterID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navTitle.title = "Characters List"
        tableView.estimatedRowHeight = 180.0
        tableView.rowHeight = UITableView.automaticDimension
        fetchData()
    }
    
    func fetchData() {
        NetManager.requestData(requestID: -1, offset: self.fromIndex, limit: self.limit) { (Character) in
            self.response = Character
            self.characters.append(contentsOf: (Character.data.results))
        } onFailure: { (error) in
            let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // MARK: -UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! MainViewTableViewCell
        let character = self.characters[indexPath.row]
        let thumbnail = character.thumbnail
        
        cell.charNameLabel.text = character.charName
        
        let imagePath = String(format: "%@%@%@", thumbnail.thumbPath, "/standard_xlarge.", thumbnail.thumbExtension)
        cell.charImage.sd_setImage(with: URL(string: imagePath), placeholderImage: UIImage(named: "placeholder"))
        
        // fetch new characters when there are 'newFetchIndex' characters left to show
        if indexPath.row == self.characters.count - self.newFetchIndex {
            if let total = self.response?.data.total {
                if total > self.characters.count {
                    fromIndex = fromIndex + self.limit
                    fetchData()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCharacterID = self.characters[indexPath.row].charID
        self.performSegue(withIdentifier: "segueToDetail", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetail" {
            if let vc = segue.destination as? DetailViewController {
                vc.characterID = self.selectedCharacterID
            }
        }
    }
}
