//
//  DetailViewController.swift
//  HiberusTest
//
//  Created by Michele Rauzino on 18/3/21.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var charImageView: UIImageView!
    @IBOutlet weak var charDescriptionTextView: UITextView!
    @IBOutlet weak var navTitle: UINavigationItem!
    
    var characterID = 0
    var response: Character?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navTitle.title = ""
        fetchData()
    }
    
    func fetchData() {
        NetManager.requestData(requestID: characterID, offset: 0, limit: 0) { (Character) in
            self.response = Character
            let character = self.response?.data.results[0]
            self.navTitle.title = character?.charName
            let thumbnail = character?.thumbnail
            let imagePath = thumbnail!.thumbPath + "/standard_xlarge." + thumbnail!.thumbExtension
            self.charImageView.sd_setImage(with: URL(string: imagePath), placeholderImage: UIImage(named: "placeholder"))
            guard let description = character?.charDescription, !description.isEmpty else {
                self.charDescriptionTextView.text = "No description provided"
                return
            }
            self.charDescriptionTextView.text = character?.charDescription
            
        } onFailure: { (error) in
            let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
