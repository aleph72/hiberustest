//
//  StartViewController.swift
//  HiberusTest
//
//  Created by Michele Rauzino on 17/3/21.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startButton.setTitle("Start", for: .normal)
        self.startButton.layer.cornerRadius = 8
        
    }
}
