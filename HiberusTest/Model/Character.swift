//
//  Character.swift
//  HiberusTest
//
//  Created by Michele Rauzino on 17/3/21.
//

struct Character: Decodable {
    let code: Int
    let attributionText: String
    let data: Datas
    
    enum CodingKeys: String, CodingKey {
        case code
        case attributionText
        case data
    }
}

struct Datas: Decodable {
    let offset: Int
    let limit: Int
    let total: Int
    let count: Int
    let results: [Result]
    
    enum CodingKeys: String, CodingKey {
        case offset
        case limit
        case total
        case count
        case results
    }
}

struct Result: Decodable {
    let charID: Int
    let charName: String
    let charDescription: String
    let thumbnail: Thumbnail
    
    enum CodingKeys: String, CodingKey {
        case charID = "id"
        case charName = "name"
        case charDescription = "description"
        case thumbnail
    }
}

struct Thumbnail: Decodable {
    let thumbPath: String
    let thumbExtension: String
    
    enum CodingKeys: String, CodingKey {
        case thumbPath = "path"
        case thumbExtension = "extension"
    }
}
